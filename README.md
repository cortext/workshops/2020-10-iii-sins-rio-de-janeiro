# 2020-10-III SINS Rio de Janeiro

Apresentação no III Simpósio Internacional Network Science, organizado pelo LARHUD (Laboratóório de Humanidades Digitais do IBICT e UFRJ) e CRIE (Centro de Inteligência Empresarial da UFRJ). Apresentação em português, outubro de 2020.

Site do simpóósio: http://networkscience.com.br/

Conteúdo da apresentação na Wikiversidade:

https://pt.wikiversity.org/wiki/Utilizador:Solstag/III_Simpósio_Internacional_Network_Science

## Programa

### Dia 22 de outubro (quinta-feira)

11h00 – 12h30 – Painel IV

#### Uso de tecnologia para análise em ciências sociais, saúde e humanidades.

Moderador – Ricardo Pimenta (IBICT)
- Utilização do Cortext para análise automática de documentos em ciências sociais, da saúde e humanidades – Alexandre Abdo (Université Gustave Eiffel – França)
- Experiências com a utilização do VosViewer e Altmetria – Rodrigo Costa (Leiden University- Holanda)
